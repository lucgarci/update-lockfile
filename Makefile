.PHONY: tests lint deploy test-cronjob

tests:
	PYTHONPATH="$(PYTHONPATH):./src" poetry run pytest \
	    -v --junitxml=report.xml --cov=src.berlin --cov-report term-missing tests/unit
	PYTHONPATH="$(PYTHONPATH):./src" poetry run coverage xml

lint:
	poetry run black ./src ./tests
	poetry run flake8 ./src ./tests
	poetry run isort ./src ./tests
