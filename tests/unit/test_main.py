import hashlib
import logging
import os
import shutil
import tempfile
from argparse import Namespace
from datetime import datetime
from pathlib import Path
from unittest.mock import MagicMock, Mock, patch

import pytest
from sh.contrib import git

import src
from src.berlin import (
    branch_is_in_remote,
    create_merge_request,
    generate_git,
    generate_lockfile_branch,
    main,
    make_lockfile_merge_request,
    remote_branch_is_up_to_date,
)


@pytest.fixture
def debug_logs(caplog):
    caplog.set_level(logging.DEBUG)
    return caplog


@pytest.fixture
def old_lockfile(request) -> Path:
    test_dir = os.path.dirname(request.module.__file__)
    return Path(f"{test_dir}/cs9-image-manifest.lock.json")


@pytest.fixture
def old_lockfile_hash(old_lockfile: Path) -> str:
    sha256sum = hashlib.sha256()
    with old_lockfile.open("rb") as lockfile:
        sha256sum.update(lockfile.read())
    return str(sha256sum.hexdigest())


@pytest.fixture
def new_lockfile(request) -> Path:
    test_dir = os.path.dirname(request.module.__file__)
    return Path(f"{test_dir}/new.1649258236.json")


@pytest.fixture
def new_lockfile_hash(new_lockfile: Path) -> str:
    sha256sum = hashlib.sha256()
    with new_lockfile.open("rb") as lockfile:
        sha256sum.update(lockfile.read())
    return str(sha256sum.hexdigest())


@pytest.fixture
def fake_repo(old_lockfile: Path) -> str:
    with tempfile.TemporaryDirectory() as repo:
        # initialize an empty repo
        git.init(str(repo))

        # add a subdirectory to the repo called package_list/
        package_list_dir = Path(repo) / "package_list"
        package_list_dir.mkdir(mode=0o755)
        # resolves to /tmp/<random>/package_list/cs9-image-manifest.lock.json
        lockfile_path = package_list_dir / old_lockfile.name

        # copy ./cs9-image-manifest.lock.json ->
        # /tmp/<some temp dir/package_list/cs9-image-manifest.lock.json
        shutil.copyfile(str(old_lockfile), str(lockfile_path))

        git.checkout("-b", "alphabet", _cwd=repo)
        git.add(lockfile_path, _cwd=repo)

        new_env = os.environ.copy()
        new_env["GIT_AUTHOR_EMAIL"] = "example@example.org"
        new_env["GIT_COMMITTER_EMAIL"] = "example@example.org"
        new_env["GIT_AUTHOR_NAME"] = "example"
        new_env["GIT_COMMITTER_NAME"] = "example"

        git.commit("-m", "test lockfile", _cwd=repo, _env=new_env)

        git.checkout("-b", "not_up_to_date", _cwd=repo)
        git.commit(
            "--allow-empty",
            "-m",
            "Empty commit on not_up_to_date",
            _cwd=repo,
            _env=new_env,
        )

        git.checkout("alphabet", _cwd=repo)
        git.commit(
            "--allow-empty",
            "-m",
            "Empty commit on alphabet",
            _cwd=repo,
            _env=new_env,
        )

        git.checkout("-b", "is_up_to_date", _cwd=repo)
        git.commit(
            "--allow-empty",
            "-m",
            "Empty commit on is_up_to_date",
            _cwd=repo,
            _env=new_env,
        )

        yield repo


@pytest.fixture
def bare_repository(fake_repo: str) -> str:
    with tempfile.TemporaryDirectory() as repo:
        git.clone("--bare", fake_repo, repo)
        git.config("receive.advertisePushOptions", "true", _cwd=repo)
        yield repo


@pytest.fixture
def working_copy(bare_repository: str) -> str:
    with tempfile.TemporaryDirectory() as local:
        git.clone(bare_repository, local)
        yield local


@pytest.mark.parametrize("expected_result", [False, True])
def test_generate_git(
    bare_repository: str,
    old_lockfile: Path,
    new_lockfile: Path,
    expected_result: bool,
) -> None:
    # if we are expecting there not to be a diff, then
    # set the new lockfile to be the same as the old one
    if not expected_result:
        new_lockfile = old_lockfile
    with tempfile.TemporaryDirectory() as working_copy:
        assert (
            generate_git(
                Path(working_copy),
                bare_repository,
                "alphabet",
                f"package_list/{old_lockfile.name}",
                str(new_lockfile),
            )
            is expected_result
        )


# from the Python docs on unittest
# we must patch datetime in the module that uses it
# not globally (i.e. we can't do @patch(datetime.datetime))
@patch("src.berlin.datetime")
@pytest.mark.parametrize(
    "lockfile_path",
    [
        "package_list/cs9-image-manifest.lock.json",
        "package_list/xxx-image-manifest.lock.json",
        "yyy.lock.json",
    ],
)
def test_generate_lockfile_branch(
    mock_datetime: Mock,
    bare_repository: str,
    lockfile_path: str,
    new_lockfile: Path,
    new_lockfile_hash: str,
) -> None:
    expected_datetime = datetime(2022, 4, 6, 15, 40)
    mock_datetime.utcnow = MagicMock(return_value=expected_datetime)
    fake_timestamp = expected_datetime.strftime("%Y-%m-%d %H:%M")

    with tempfile.TemporaryDirectory() as working_copy:
        generate_git(
            Path(working_copy),
            bare_repository,
            "alphabet",
            lockfile_path,
            str(new_lockfile),
        )

        branch_name, commit_msg = generate_lockfile_branch(
            Path(working_copy), lockfile_path
        )

        short_hash = new_lockfile_hash[0:8]
        expected_branch_name = f"lockfile_update_{short_hash}"
        assert branch_name == expected_branch_name

        expected_commit_msg = (
            f"{Path(lockfile_path).name}: update for {fake_timestamp}"
        )
        assert commit_msg == expected_commit_msg

        # ensure our branch was actually created
        branch_list = git.branch("--list", _cwd=working_copy)
        assert branch_name in branch_list


@pytest.mark.parametrize(
    "branch,expected_result", [("alphabet", True), ("doesntexist", False)]
)
def test_branch_is_in_remote(
    branch: str, expected_result: bool, working_copy: str
) -> None:
    assert branch_is_in_remote(Path(working_copy), branch) is expected_result


@pytest.mark.parametrize(
    "branch,expected_result",
    [("is_up_to_date", True), ("not_up_to_date", False)],
)
def test_remote_branch_is_up_to_date(
    branch: str, expected_result: bool, working_copy: str
) -> None:
    actual_result = remote_branch_is_up_to_date(
        Path(working_copy), branch, "alphabet"
    )
    assert actual_result is expected_result


@patch("src.berlin.datetime")
def test_create_merge_request(
    mock_datetime: Mock,
    bare_repository: str,
    working_copy: str,
    old_lockfile: Path,
    new_lockfile: Path,
    new_lockfile_hash: str,
) -> None:
    expected_datetime = datetime(2022, 4, 6, 15, 40)
    mock_datetime.utcnow = MagicMock(return_value=expected_datetime)

    with tempfile.TemporaryDirectory() as working_copy:
        generate_git(
            Path(working_copy),
            bare_repository,
            "alphabet",
            f"package_list/{old_lockfile.name}",
            str(new_lockfile),
        )

        lockfile_path = f"package_list/{old_lockfile.name}"
        branch_name, commit_msg = generate_lockfile_branch(
            Path(working_copy), lockfile_path
        )

        assert (
            create_merge_request(
                Path(working_copy),
                branch_name,
                target_branch="alphabet",
                labels="automated-updates",
                title=commit_msg,
            )
            is None
        )


@patch("src.berlin.generate_git")
def test_make_lockfile_merge_request_no_diff(
    mock_generate_git: Mock,
    bare_repository: str,
    old_lockfile: Path,
    new_lockfile: Path,
    debug_logs,
) -> None:
    mock_generate_git.return_value = False

    expected_logs = [
        "Cloning git and applying the new lockfile",
        "No change in lockfile",
    ]

    make_lockfile_merge_request(
        bare_repository,
        "alphabet",
        f"package_list/{old_lockfile.name}",
        str(new_lockfile),
        False,
    )

    for line in expected_logs:
        assert line in debug_logs.text, f"Logs are missing {line}"


@pytest.mark.parametrize(
    "branch_in_remote,remote_up_to_date,message",
    [
        (True, True, "Branch already exists and is up to date."),
        (True, False, "Remote branch exists but is not up to date"),
        (False, None, "Branch does not exist in remote"),
    ],
)
def test_make_lockfile_merge_request(
    branch_in_remote: bool,
    remote_up_to_date: bool,
    message: str,
    bare_repository: str,
    old_lockfile: Path,
    new_lockfile: Path,
    new_lockfile_hash: str,
    debug_logs,
):
    expected_branch_name = f"lockfile_update_{new_lockfile_hash[0:8]}"
    expected_commit_message = "lockfile update"
    expected_tempdir = tempfile.TemporaryDirectory()
    expected_repo_path = Path(expected_tempdir.name) / "repo"

    mock_generate_git = patch("src.berlin.generate_git").start()
    mock_generate_git.return_value = True

    mock_generate_lockfile_branch = patch(
        "src.berlin.generate_lockfile_branch"
    ).start()
    mock_generate_lockfile_branch.return_value = (
        expected_branch_name,
        expected_commit_message,
    )

    mock_branch_in_remote = patch("src.berlin.branch_is_in_remote").start()
    mock_branch_in_remote.return_value = branch_in_remote

    mock_remote_branch_up_to_date = patch(
        "src.berlin.remote_branch_is_up_to_date"
    ).start()
    mock_remote_branch_up_to_date.return_value = remote_up_to_date

    mock_create_merge_request = patch(
        "src.berlin.create_merge_request"
    ).start()

    with patch("tempfile.TemporaryDirectory") as mock_tempfile:
        mock_tempfile.return_value = expected_tempdir
        make_lockfile_merge_request(
            bare_repository,
            "alphabet",
            f"package_list/{old_lockfile.name}",
            str(new_lockfile),
            False,
        )

    expected_logs = [
        "Cloning git and applying the new lockfile",
        "Creating unique lockfile branch",
        "Checking remote branch status",
        message,
    ]

    if branch_in_remote and not remote_up_to_date:
        expected_logs.append("Creating merge request")

    for line in expected_logs:
        assert line in debug_logs.text, f"Logs are missing {line}"

    if branch_in_remote and not remote_up_to_date:
        mock_create_merge_request.assert_called_once_with(
            expected_repo_path,
            expected_branch_name,
            target_branch="alphabet",
            labels=["automated-updates"],
            title=expected_commit_message,
        )

    patch.stopall()


@patch("shutil.which", create=True)
def test_main_git_not_available(mock_which) -> None:
    mock_which.return_value = ""
    with pytest.raises(SystemExit):
        main()


@patch("src.berlin.make_lockfile_merge_request")
@patch("subprocess.run", create=True)
@patch("shutil.which", create=True)
@patch("time.time")
@patch("tempfile.gettempdir")
@patch("argparse.ArgumentParser.parse_args")
@pytest.mark.parametrize(
    "make_lockfile_distro,make_lockfile_repos",
    [
        (None, None),
        ("x", "a b c"),
        ("y", "a b c"),
    ],
)
def test_main(
    mock_parse_args,
    mock_tempdir,
    mock_time,
    mock_which,
    mock_subprocess_run,
    mock_make_lock_file_merge_request,
    request,
    make_lockfile_distro,
    make_lockfile_repos,
):
    # arrange
    here = Path(request.module.__file__).parent
    # (project_dir/src/berlin.py).parent.parent -> project_dir
    project_dir = Path(src.berlin.__file__).parent.parent

    # when main calls parse_args on our ArgumentParser return
    # some expected values
    fake_args = Namespace(
        repo="https://gitlab.com/CentOS/automotive/sample-images.git",
        branch="main",
        lockfile_path="package_list/cs9-image-manifest.lock.json",
        dry_run=False,
    )
    mock_parse_args.return_value = fake_args

    # when main calls tempfile.gettempdir, return this directory
    # since this is where new.{timestamp}.json lives
    mock_tempdir.return_value = here

    # when main calls time.time to generate the new file name
    # return an expected value we can test against
    expected_time = 1649258236.0
    mock_time.return_value = expected_time

    # when main calls shutil.which, return an expected value
    # which is a standard place for git on Linux
    mock_which.return_value = "/usr/bin/git"

    # when main calls subprocess.run, return an exit code
    # for a successful run, without actually running it
    mock_subprocess_run.return_value = 0

    # Mock some environment variables
    if make_lockfile_distro:
        os.environ["MAKE_LOCKFILE_DISTRO"] = make_lockfile_distro
    if make_lockfile_repos:
        os.environ["MAKE_LOCKFILE_REPOS"] = make_lockfile_repos

    # act
    main()

    # assert
    mock_which.assert_called_once_with("git")

    expected_new_lockfile = here / f"new.{int(expected_time)}.json"
    expected_cmd = [
        "python3",
        str(project_dir / "make-lockfile" / "make-lockfile.py"),
        "--outfile",
        str(expected_new_lockfile),
    ]
    if make_lockfile_distro:
        expected_cmd.extend(["--distro", make_lockfile_distro])
    if make_lockfile_repos:
        expected_cmd.extend(["--repos"] + make_lockfile_repos.split())
    mock_subprocess_run.assert_called_once_with(
        expected_cmd,
        check=True,
    )

    mock_make_lock_file_merge_request.assert_called_once_with(
        fake_args.repo,
        fake_args.branch,
        fake_args.lockfile_path,
        str(expected_new_lockfile),
        fake_args.dry_run,
    )
